//Create Auth Middleware with check if authorization header exists or not. If not you must send res status 403 with "No credentials" message
exports.authorizeHeader = (req, res, next) => {
  req.header('Authorization') ? next() : res.status(403).json('No credentials');
};

exports.authorizeUser = (req, res, next) => {
  if (req.header('Authorization') === '790f2b152ad1010b8db473b206bb6725') {
    res.status(401).json({ message: 'Unauthorized' });
  }
  if (req.header('Authorization') === '0972164370bb3a0c266fbd18c09c9e87') {
    req.customAuthHeader = '0972164370bb3a0c266fbd18c09c9e87';
  }
  next();
};

// module.exports = { authorizeHeader, authorizeUser };

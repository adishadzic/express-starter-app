const express = require('express');
const app = express();
const morgan = require('morgan');
const md5 = require('md5');

const authorizationHeader = require('./middleware/auth');
const customModule = require('./custom_module');

const port = 8002;

app.use(express.json());
app.use(morgan('combined'));

app.get('/', (req, res) => {
  res.send('<h1>My first express app - Author: Adis Hadzic</h1>');
});

app.get('/not-found', (req, res) => {
  res.status(404).send('<h3>Sorry. Page is not found</h3>');
});

app.get('/error', (req, res, next) => {
  try {
    console.log('Error handling');
    throw new Error('Hello Error!');
  } catch (error) {
    next(error);
  }
});

app.get('/student-data', (req, res) => {
  const { firstName, lastName, email } = req.query;

  let concatenatedString = customModule.concat(firstName, lastName, email);

  res.json({ concatenatedString });
});

app.post('/authorization', async (req, res, next) => {
  try {
    const { firstName, lastName, id, email } = req.body;
    let concatenatedString = id.concat('_' + firstName + '_' + lastName + '_' + email);
    const hashedData = md5(concatenatedString);

    res.setHeader('Authorization', hashedData);
    res.json(hashedData);
  } catch (error) {
    next(error);
  }
});

app.get(
  '/private-route',
  authorizationHeader.authorizeHeader,
  authorizationHeader.authorizeUser,
  (req, res) => {
    req.customAuthHeader
      ? res.json({ authHeaderExist: true, value: req.customAuthHeader })
      : res.json({ authHeaderExist: false, value: '' });
  }
);

app.use(function (err, req, res, next) {
  console.error(err.message);
  res.status(500).send('Error!');
});

app.listen(port, () => {
  console.log(`Server running on port: http://localhost:${port}`);
});
